public class kata2 {
    public static void main(String[] args) {
        String[] deckSteve = {"9", "7", "7", "8", "A"};
        String[] deckJosh = {"7", "7", "7", "5", "T"};
        System.out.println(winner(deckSteve, deckJosh));
    }
    public static String winner(String[] deckSteve, String[] deckJosh) {
        //If variable winner stay in 0 is tie
        int winner = 0;
        //x for Steve score's
        int x = 0;
        //y for Josh score's
        int y = 0;

        //Convert Characters to Numbers
        deckSteve = toNumericValues(deckSteve);
        deckJosh = toNumericValues(deckJosh);


        for(int i = 0; i < deckSteve.length; i++){
            if(Integer.parseInt(deckSteve[i]) > Integer.parseInt(deckJosh[i])){
                x++;
            }else if(Integer.parseInt(deckSteve[i]) < Integer.parseInt(deckJosh[i])){
                y++;
            }
        }

        winner = compareScores(x,y);

        if(winner == 1){
            return "Steve wins "+x+ " to "+y;
        }else if(winner == 2){
            return "Josh wins "+y+ " to "+x;
        }else{
            return "Tie";
        }
    }

    public static String[] toNumericValues(String[] deck){
        for(int i = 0; i < deck.length; i++){
            if(deck[i] == "T"){
                deck[i] = "10";
            } else if(deck[i] == "J"){
                deck[i] = "11";
            }else if(deck[i] == "Q"){
                deck[i] = "12";
            }else if(deck[i] == "K"){
                deck[i] = "13";
            }else if(deck[i] == "A"){
                deck[i] = "14";
            }
        }
        return deck;
    }
    public static int compareScores(int x, int y){
        int winner = 0;
        if(x>y){
            winner = 1;
        }else if(x<y){
            winner = 2;
        }
        return winner;
    }
}
