public class kata1 {
    public static void main(String[] args) {
        String text = "Hola";
        System.out.println(solution(text));
    }
    public static String solution(String str) {
        String result = "";
        for(int i = 0; i < str.length(); i++){
            result = str.charAt(i) + result;
        }
        return result;
    }
}
